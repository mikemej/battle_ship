#!/usr/bin/python
# -*- coding: utf-8 -*-
########################################################
__author__ = "Michael Mejias"
__copyright__ = "Copyright 2022, mike.test"
__credits__ = ["Michael Mejias"]
__license__ = "None"
__version__ = "0.1"
__maintainer__ = "Michael Mejias"
__email__ = "mejias.michael1@gmail.com"
__status__ = "Beta"
########################################################
#importing Packages
import time
import click

#Local Variables


#Functions
def show_window(text):
    '''Setting a proper window for every print 20 lines as max'''
    text_len = len(text.splitlines())
    total_max_window = 20
    init_text = ((total_max_window / 2) + 1 ) - text_len
    end_range = total_max_window - text_len
    print('='*100)
    time.sleep(1)
    for line in range(0,end_range):
        if line == init_text:
            for new_line in text.splitlines():
                print(new_line)
        else:
            print()
    time.sleep(1)
    print('='*100)
    time.sleep(3)
    click.clear()



def welcome():
    '''Welcoming the players'''
    text = '''
    Welcome
    to
    Battle Ship
    The Game...
    '''
    show_window(text)

def good_bye():
    '''Saying good bye to the players'''
    text = '''
    Thank you so much
    for playing
    Battle Ship
    The Game
    Good to have you 
    Good Bye ...
    '''
    show_window(text)

welcome()
good_bye()
#setting the ships
game_board = list(range(1,57))
init_board = 0
line_of_board = 8
ships = {
    'name' : 'Carrier',
    'positions' : [30,31,32,33,34,35],
    'number_positions': 5,
    'hitted' : False,
    'number_hitted': 0,
    'damage' : 0,
    'destroyed' : False
},{
    'name' : 'battle_ship', 
    'positions' : [20,21,22,23],
    'number_positions': 4,
    'hitted' : False,
    'number_hitted': 0,
    'damage' : 0,
    'destroyed' : False
},{
    'name' : 'cruiser',
    'positions' : [11,12,13],
    'number_positions': 3,
    'hitted' : False,
    'number_hitted': 0,
    'damage' : 0,
    'destroyed' : False
},{
    'name' : 'submarine',
    'positions' : [7,8,9],
    'number_positions': 3,
    'hitted' : False,
    'number_hitted': 0,
    'damage' : 0,
    'destroyed' : False
},{
    'name' : 'destroyer',
    'positions' : [1,2],
    'number_positions': 2,
    'hitted' : False,
    'number_hitted': 0,
    'damage' : 0,
    'destroyed' : False
}
def deploy_a_ship(ship):
    '''The function will be use to deploy a ship on the game board'''
    positions_value = ship['positions']
    position_list = []
    print('Please proceed to deploy the Ship: ', ship['name'], 'on the gameboar')
    print('The ship has the following long in squares: ', ship['number_positions'])
    while len(position_list) != ship['number_position'] and validate_positions(position_list, ship['number_positions']) != True:
        print('Insert a valid position for one square of the ship')
        position_list.append(int(input()))


def validate_positions(pos_list, number_positions):
    if len(pos_list) != number_positions:
        print(False)
        return False
    pos_list.sort()
    print(pos_list)
    iterator_horizontal = 1
    iterator_vertical = 8
    aux = False
    while iterator_horizontal < number_positions:
        if (pos_list[0] + iterator_horizontal) in pos_list:
            aux = True
        elif (pos_list[0] + iterator_vertical) in pos_list:
            aux = True
        else:
            aux = False
            print(aux)
            return False
        iterator_horizontal += 1
        iterator_vertical += 8
    if aux:
        print(aux)
        return True     
     

def update_stats(position):
    for ship in ships:
        print(ship['positions'])
        if position in ship['positions']:
            ship['hitted'] = True
            ship['number_hitted'] += 1
            ship['damage'] = (ship['number_hitted'] / ship['number_positions']) * 100
            if ship['damage'] > 99:
                ship['destroyed'] = True

def printing_updates():
    for ship in ships:
        if ship['hitted'] == True:
            print(ship)

def show_board():
    global line_of_board, init_board

    lines = []
    while init_board < len(game_board):
        lines.extend(game_board[init_board:line_of_board])
        lines.extend('\n\r')
        time.sleep(0.5)
        line_of_board += 8
        init_board += 8
    show_window(str(lines))

show_board()   
#time.sleep(5)
#validate_positions([6, 0], 2) #Flase Horizontal
#validate_positions([4, 5], 2) #True Horizontal
#validate_positions([2, 10, 12], 3) #False Vertical
#validate_positions([2, 10, 18, 26, 34], 5) #True Vertical
#validate_positions([4, 5, 6, 7], 4) #Flase More elements
#validate_positions([3, 4, 5], 3) #False Less elements
#click.clear()    
#update_stats(1)
#printing_updates()
#update_stats(8)
#printing_updates()
#update_stats(8)
#printing_updates()